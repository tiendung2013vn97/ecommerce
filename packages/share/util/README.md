# Common Utils for projects
**This include 3 main utils:
* **Validator: joi validate with custom message
* **Constants: default constants for all services
* **Jwt: jsonwebtoken encode/decode
