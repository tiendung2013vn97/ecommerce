const jwt = require('jsonwebtoken');
const { config } = require('./config');
const { logger } = require('@ntiendung/logger')
const randtoken = require('rand-token');

const LOGIN_TTL = 60 * 10

async function encrypt(data, ttl = LOGIN_TTL, iat = Math.floor(Date.now() / 1000)) {
    return new Promise((resolve, reject) => {
        const exp = Math.floor(iat + ttl)
        console.log("config.get('jwt')",config.get('jwt'))
        jwt.sign({ ...data, iat, exp }, config.get('jwt.privateKey'), { algorithm: 'RS256' }, function (err, token) {
            if (err) {
                logger.error(`Failed to encrypt data.`, { Reason: err })
                return reject(`Failed to encrypt data`)
            }
            return resolve(token)
        });
    })
}

async function decrypt(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.get('jwt.publicKey'), function (err, decoded) {
            if (err) {
                if (err.message === 'jwt expired') return reject('TokenExpiredError')
                logger.error(`Failed to decrypt token.`, { Reason: err })
                return reject(`Failed to decrypt token`)
            }
            return resolve(decoded)
        });
    })
}

function createRefreshToken(token) {
    return randtoken.generate(64);
}

module.exports = {
    encrypt, decrypt, createRefreshToken
}