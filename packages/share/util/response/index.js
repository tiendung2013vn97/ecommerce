const messageUtil = require('./message')

/**
 * Create new Error Response
 * @param {*} detail 
 * @param {*} msg 
 * @param {*} code 
 * @param {*} statusCode 
 * @returns 
 */
function fail(detail, msg, code, statusCode = 400) {
    return {
        status: "fail",
        detail,
        msg,
        code,
        statusCode,
        service: process.env.SERVICE_NAME
    }
}

/**
 * Get Error Response by  errCode
 * @param {*} errCode 
 * @param {*} params 
 * @param {*} detail 
 * @param {*} statusCode 
 * @returns 
 */
function getErrResp(errCode, params = [], detail, statusCode = 400) {
    const message = messageUtil.get(errCode, params)
    return fail(detail, message, errCode, statusCode)
}

/**
 * Check if error is an instance of service's Error Response (not belongs to external-service) 
 * @param {*} error 
 * @returns 
 */
function isServiceErrResp(error) {
    try {
        const serviceErrorKeys = Object.keys(fail())
        const errorKeys = Object.keys(error)

        return serviceErrorKeys.sort().join() == errorKeys.sort().join()
    } catch (error) {
        return false
    }
}

module.exports = {
    fail,
    getErrResp,
    isServiceErrResp,
    messageUtil
}