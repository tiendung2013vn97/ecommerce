/**
 * Winston logger which integrates with express to capture information about the http request.
 *
 * @param logger - Winston logger instance.
 * @param format (optional) - Object containing format options (see below).
 * @return {Function}
 */
function createLoggerMdw(logger, format) {
    return (req, res, next) => {
        const requestEnd = res.end;
        const startTime = new Date();

        // Proxy the real end function
        res.end = (chunk, encoding) => {
            // Our format argument above contains key-value pairs for the output
            // object we send to Winston. Let's use this to format our results:
            const data = {};
            const tokens = {
                ':date': startTime.toISOString(),
                ':statusCode': res.statusCode,
                ':method': req.method,
                ':responseTime': new Date() - startTime,
                ':url': req.protocol + '://' + req.get('host') + req.originalUrl,
                ':ip': req.headers['x-forwarded-for'] || req.ip || req.connection.remoteAddress,
                ':userAgent': `${req.get('User-Agent')}`
            };

            // Do the work expected
            res.end = requestEnd;
            res.end(chunk, encoding);

            // Let's define a default format
            if (typeof format !== 'object') {
                format = {
                    date: ':date',
                    status: ':statusCode',
                    method: ':method',
                    url: ':url',
                    response_time: ':responseTime',
                    user_agent: ':userAgent',
                    ip: ':ip'
                };
            }

            // ... and replace our tokens!
            const replaceToken = str => {
                return tokens[str];
            };
            for (const key in format) {
                data[key] = format[key];
                for (const token in tokens) {
                    data[key] = data[key].replace(
                        new RegExp(token),
                        typeof tokens[token] === 'function' ? tokens[token] : replaceToken
                    );
                }
            }

            logger.http(
                JSON.stringify({
                    method: data.method,
                    status: data.status,
                    url: data.url,
                    userAgent: data.user_agent,
                    responseTime: data.response_time,
                    ip: data.ip
                })
            );
        };

        next();
    };
};

module.exports = {
    createLoggerMdw
}
