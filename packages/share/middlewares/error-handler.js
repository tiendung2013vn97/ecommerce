// Error handler middleware
// handle unexpected or non-trycatch exceptions
const { logger } = require("@ntiendung/logger")
const { response, constants } = require('@ntiendung/util')

module.exports = (error, req, res, _) => {
    // return error response of handler
    if (response.isServiceErrResp(error)) {
        delete error.status
        return res.status(error.statusCode).send(error)
    }

    // uncatch error of handler
    logger.error(`An unexpected error happened! Reason: ${JSON.stringify(error)}`);
    let errorObj = response.getErrResp(constants.UNEXPECTED_ERROR, undefined, undefined, 500);
    delete errorObj.detail
    delete errorObj.statusCode;
    return res.status(500).send(errorObj);
}
