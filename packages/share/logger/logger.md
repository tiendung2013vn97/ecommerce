# Logger Usage
**1. Function: logger[<logLevel>](<message: string|any>, <params: object>)**
* **logLevel**: log level you want to use (must in **http/error/warn/info/verbose/debug/silly**)
* **message**: The message you want to log
* **params**: Use this parameter if you want to log more values or override default log. Default log contains some infomation as bellow:
  * **environment**: development/staging/production 
  * **type**: log type (default is CommonLogger), if you want to change another value then updating params.type=<yourLoggerType> 
  * **timestamp**: logging start time 
  * **requestId**: each http request was generated an uuid 
  * **userId**: api caller 
  * **clientId**: get from req.headers.clientId 
  * **logLevel**: log level as http/error/warn/info/verbose/debug/silly 
  * **service**: service name (ex: Tabula API Service) 
  * **requestInfo**: request information as body/url/ip/... (exist if logLevel is "error" and in a http context)
  
**2. Example:**
    Import { logger } from 'app/config/logger'
    logger.info('This is my message!', { name: 'David'} );
    => {"message": "This is my message!", "name": "David", "logLevel": "info", "environment": "development",....}