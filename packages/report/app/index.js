global.ROOT = __dirname
const { loadConf } = require("@ntiendung/util").config
const { httpLoggerMdw, errorHandlerMdw, authorizationMdw } = require('@ntiendung/middlewares')
const { httpLogger } = require('@ntiendung/logger')
const express = require('express');
const app = express();
const cors = require("cors")
const helmet = require('helmet')
const path = require('path');
const httpContext = require('express-http-context');
const authRoutes = require('./modules/config/auth-routes')
const cookieParser = require('cookie-parser');
const { messageUtil } = require('@ntiendung/util').response
const { kafkaConsumer } = require('./modules/kafka')

//use cors middleware to avoid error CROSS DOMAIN
app.use(helmet())
app.use(cors())
app.use(cookieParser());
app.use(httpContext.middleware);

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false, limit: '50mb' }));

app.use(express.static(path.join(__dirname, '../public')));

// init base configurations
loadConf({ loadFromDb: false }).then(async () => {
  //load more message
  messageUtil.load(require('./modules/config/messages'))

  app.use(httpLoggerMdw.createLoggerMdw(httpLogger));
  app.use(authorizationMdw.checkAuthorization(authRoutes))

  //wait 2s for product service create topics if topic doesn't exist then init kafka consumer
  setTimeout(() => {
    kafkaConsumer.initConsumer()
  }, 30000)


  // load route
  const VERSION = process.env.VERSION
  app.use(`/api/${VERSION}`, require("./modules/routes"));

  //the latest layer to handle response before sending to client
  app.use(errorHandlerMdw)
})

module.exports = app;
