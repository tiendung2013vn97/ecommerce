
const { Client } = require('@elastic/elasticsearch');
const { logger } = require("@ntiendung/logger")

let es
//_________________________________________________________________________________________________
//config elasticsearch connection
async function initElastic() {
    try {
        logger.info(`Start initializing elasticsearch credentials at ${process.env.ES_DOMAIN}...`)

        es = new Client({
            node: process.env.ES_DOMAIN,
            maxRetries: 5,
            requestTimeout: 5000,
            sniffOnStart: false
        });
        await es.indices.getSettings()
        logger.info(`Setup elasticsearch credentials done!`)
    } catch (error) {
        logger.error(`Failed to initialize elasticsearch. `, { Reason: error })
        throw `Failed to initialize elasticsearch`
    }
}

//init elasticsearch settings, exit program if failed
initElastic().catch(() => process.env.NODE_ENV == "production" && process.exit(1))

//_________________________________________________________________________________________________
async function getSearchActivites(body) {
    return es.search({
        index: process.env.ES_SEARCH_ACTIVITY, body
    }).then(result => ({
        searchActivities: result.body.hits.hits.map(item => item._source),
        from: body.from,
        size: body.size,
        total: result.body.hits.total.value
    }))
}

//_________________________________________________________________________________________________
async function addSearchActivity(activity) {
    return es.index({
        index: process.env.ES_SEARCH_ACTIVITY,
        body: activity
    })
}


module.exports = {
    getSearchActivites,
    addSearchActivity
}
