const express = require("express");
const { asyncHandler } = require('@ntiendung/util');
const router = express.Router();
const handler = require("./handler")
const { validateQuerySearchActivityReq } = require('./validator')

//validate request
router.post("/query", validateQuerySearchActivityReq)

//get report of user's searching activities
router.post("/query", asyncHandler(async (req, res, _) => handler.getSearchActivites(req)))

module.exports = router