const es = require('../../elasticsearch')
const { kafkaConsumer } = require('../../kafka')
const { config } = require('@ntiendung/util').config
const { logger } = require('@ntiendung/logger')

kafkaConsumer.registerTopic(config.get('kafka.consumer.fetch_requests')[0].topic, msg => {
    es.addSearchActivity(JSON.parse(msg))
        .catch(err => logger.error(`Failed to add search activity`, { Reason: err }))
})

async function getSearchActivites(req) {
    return es.getSearchActivites(req.body)
}

module.exports = {
    getSearchActivites
}