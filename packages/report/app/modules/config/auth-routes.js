module.exports = [
    //search activities
    {
        method: 'post',
        path: '/report/search-activities/query',
        requireAccessToken: 'optional' //enum [mandatory,optional]
    }
]