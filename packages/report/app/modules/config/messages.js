const constants = require('./constants')

module.exports = {
    [constants.PRODUCT_NOT_FOUND]: `Product not found`
}