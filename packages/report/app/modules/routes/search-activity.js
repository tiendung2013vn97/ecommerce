const express = require("express");
const router = express.Router();

router.use("/", require("../services/search-activity/controller"))

module.exports = router;