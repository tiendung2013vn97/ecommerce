const kafka = require("kafka-node");
const { config } = require('@ntiendung/util').config;
const { logger } = require('@ntiendung/logger')
const Consumer = kafka.Consumer;
const handlers = [];

function initConsumer() {
    const client = new kafka.KafkaClient({ kafkaHost: config.get('kafka.server.kafkaHost') });
    const kafkaConsumer = new Consumer(
        client,
        config.get('kafka.consumer.fetch_requests'),
        config.get('kafka.consumer.option')
    );

    kafkaConsumer.on("error", function (err) {
        logger.error(`Error from kafka.`, { Reason: err });
    });

    kafkaConsumer.on("message", msg => {
        const processors = handlers.filter(handler => handler.topic === msg.topic)
        processors.forEach(processor => processor.handle(msg.value))
    })
    logger.info(`Kafka consumer initialized!`)
}


const registerTopic = ((topic, handle) => handlers.push({ topic, handle }))


module.exports = { initConsumer, registerTopic }