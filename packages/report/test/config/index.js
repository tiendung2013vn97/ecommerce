const { loadConf } = require("@ntiendung/util").config
loadConf({ loadFromDb: false })

module.exports = {
    elastic: {
        domain: "http://localhost:9200",
        username: null,
        password: null
    },
    searchIndex: process.env.ES_SEARCH_ACTIVITY
}