const { testUnit } = require("../run")
const modulePath = "app/modules/services/search-activity/handler"

describe("Product testcases", async () => {
    testUnit("Search user activities successfully", { modulePath, funcName: "getSearchActivites", key: "search-activity/get-search-activities-v1" })
})
