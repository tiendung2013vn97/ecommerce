const nock = require('nock');

module.exports = {
    mock: function (data) {
        nock.disableNetConnect()
        const { action, output } = data
        nock(action.baseUrl)[action.method](action.path)
            .reply(output.code, output.data)
    },
    unmock: function () {
        nock.cleanAll()
        nock.enableNetConnect()
    }
}