const config = require("../../config")
module.exports = {
    "input": {
        "args": [
            {
                "body": {
                    "from": 0,
                    "size": 1
                }
            }
        ]
    },
    "output": {
        "data": {
            searchActivities: [
                {
                    "type": "SEARCH_PRODUCTS",
                    "url": "/api/v1/products/search",
                    "userId": "user-id-123",
                    "ip": "::ffff:172.29.64.1",
                    "method": "POST",
                    "date": "2021-09-15T17:13:31.174Z",
                    "userAgent": "PostmanRuntime/7.26.8",
                    "query": {
                        "from": 0,
                        "size": 1
                    }
                }
            ],
            "from": 0,
            "size": 1,
            "total": 10

        },
        "isErr": false
    },
    "mocks": [
        {
            "service": "api",
            "action": {
                "baseUrl": config.elastic.domain,
                "method": "post",
                "path": `/${config.searchIndex}/_search`
            },
            "output": {
                "code": 200,
                "data": {
                    "hits": {
                        "total": {
                            "value": 10,
                            "relation": "eq"
                        },
                        "max_score": 1.0,
                        "hits": [
                            {
                                "_index": "search_activity",
                                "_type": "_doc",
                                "_id": "50Bz6nsBj51oBZicAyk0",
                                "_score": 1.0,
                                "_source": {
                                    "type": "SEARCH_PRODUCTS",
                                    "url": "/api/v1/products/search",
                                    "userId": "user-id-123",
                                    "ip": "::ffff:172.29.64.1",
                                    "method": "POST",
                                    "date": "2021-09-15T17:13:31.174Z",
                                    "userAgent": "PostmanRuntime/7.26.8",
                                    "query": {
                                        "from": 0,
                                        "size": 1
                                    }
                                }
                            }]
                    }
                }
            }
        }
    ]
}