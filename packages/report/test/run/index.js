const { before } = require('mocha')
const path = require('path')
const { beforeAll } = require("../mock");

//before run first testcase
before(function () {
    //base dir of service
    global.BASE_DIR = path.resolve(path.join(__dirname, "../../"))

    //initialize mock setup
    beforeAll()

    //hook services
    require("../hooks")
})

module.exports = {
    testUnit: require("./unit-test")
}