// eslint-disable-next-line no-unused-vars
const { beforeEach, afterEach, it, before, after } = require('mocha')
const joi = require("joi")
const path = require('path')
const logger = require("../logger")
const rewire = require("rewire");
const chai = require("chai")
const ex = chai.expect
const { mock, unmock } = require("../mock");

//disable truncating when length of expected result is too long
chai.config.truncateThreshold = 0;

const testUnit = function (description, option) {
    return it(description, async () => {
        const { func, input, output, mocks } = extractOpt(option)
        //mock before run testcase
        await mock(mocks)

        //run function
        let result
        let isErr = false
        try {
            result = await func(...input.args || [])
        } catch (error) {
            isErr = true
            result = error
            if (!output.isErr) logger.error(`NonExpectedError:`,error)
        }

        if (output.normalize) result = normalize(result)

        //unmock after function finished
        await unmock(mocks)

        //compare real result with expected output
        ex(isErr).to.eql(output.isErr == undefined ? false : output.isErr)

        //normalize result then verify
        if (output.data instanceof Function) output.data = output.data(result)
        ex(result).to.deep.eql(output.data)
    })
}


function extractOpt(option) {
    //validate option
    const validation = joi.object({
        modulePath: joi.string().required(),
        funcName: joi.string().required(),
        key: joi.string().required()
    }).required().validate(option)

    if (validation.error) {
        logger.error("INVALID OPTION:", validation.error)
        throw "INVALID OPTION"
    }

    //extract opt
    let { modulePath, funcName, key } = option

    //get input and expected output of testcase
    const { input, output, mocks = [] } = require(path.join(__dirname, "../keys/", key))
    if (!input || !output) throw (`input and output of testcase are required.`)

    //get function from module
    const module = rewire(path.join(global.BASE_DIR, modulePath));
    let func
    try {
        func = module.__get__(funcName)
    } catch (error) {
        func = module[funcName]
    }
    if (!func) throw `Function ${funcName} doesn't exist in module ${modulePath}`

    return {
        func,
        input,
        output,
        mocks
    }
}

function normalize(object) { return JSON.parse(JSON.stringify(object)) }

module.exports = testUnit