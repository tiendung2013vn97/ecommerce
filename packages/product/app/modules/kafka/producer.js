const kafka = require("kafka-node");
const { config } = require('@ntiendung/util').config
const { logger } = require('@ntiendung/logger')
const Producer = kafka.Producer;

let client, kafkaProducer
let isReady = false;

const send = (topic, messages) => {
  const payloads = [
    {
      topic: topic,
      messages: JSON.stringify(messages)
    }
  ];

  if (isReady) {
    kafkaProducer.send(payloads, function (err, data) {
      if (err) {
        logger.error(`[Kafka] Can't send data because: `, { Reason: err });
      } else {
        logger.debug("kafka-producer -> data is sent successfully: ", { data });
      }
    });
  } else {
    logger.error("kafka-producer -> not ready to send!");
  }
};

const createTopic = (topicsConfig) => {
  client.connect()
  return new Promise((resolve, reject) => {
    client.createTopics(topicsConfig, (error, result) => {
      // result is an array of any errors if a given topic could not be created
      if (error)
        reject(error);
      else
        resolve(result);
    })
  })
}

const disconnect = () => {
  client.close()
}

function initProducer() {  
  client = new kafka.KafkaClient({ kafkaHost: config.get('kafka.server.kafkaHost') });
  kafkaProducer = new Producer(client)

  kafkaProducer.on('ready', async function () {
    logger.debug("kafka-producer -> ready!");
    isReady = true;

    client.createTopics(config.get('kafka.producer.topicDefault'), (error, result) => {
      // result is an array of any errors if a given topic could not be created
      if (error)
        logger.error(`Failed to create kafka topic`, { Reason: error });
      else
        logger.debug(`Create kafka topic successfully.`);
    });
  })

  kafkaProducer.on("error", function (err) {
    logger.error(`kafka-producer -> `, { Reason: err });
    isReady = false;
  });
}

module.exports = { send, createTopic, disconnect, initProducer }