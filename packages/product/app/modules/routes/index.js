const express = require("express")
const router = express.Router()

router.use("/products", require("./product"))

// define healthcheck url
router.get('/healthz', function (req, res) {
    // do app logic here to determine if app is truly healthy
    // return 200 if healthy, and anything else will fail

    res.send({
        commit_id: process.env.COMMIT_ID,
        build_time: process.env.BUILD_TIME,
        branch: process.env.BRANCH,
        env: process.env.ENV,
    })
});

module.exports = router