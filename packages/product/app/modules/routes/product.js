const express = require("express");
const router = express.Router();

router.use("/", require("../services/product/controller"))

module.exports = router;