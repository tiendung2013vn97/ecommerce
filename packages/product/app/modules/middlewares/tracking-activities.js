const { CommonUtil } = require('@ntiendung/util')
const { config } = require('@ntiendung/util').config
const { kafkaProducer } = require('../kafka')
const constants = require('../config/constants')


module.exports = (req, res, next) => {
    for (route of trackingRoutes) {
        const fullRoute = `${process.env.BASE_ROUTE}${route.path}`
        if (CommonUtil.matchPath(fullRoute, req.path) && req.method.toLowerCase() === route.method.toLowerCase()) {
            const activity = {
                type: route.type,
                url: fullRoute,
                userId: req.user?.userId,
                ip: req.headers['x-forwarded-for'] || req.ip || req.connection.remoteAddress,
                method: req.method,
                date: new Date().toISOString(),
                userAgent: `${req.get('User-Agent')}`
            }

            switch (route.type) {
                case constants.GET_PRODUCT_BY_ID: {
                    activity.productId = req.params.productId
                    break
                }
                case constants.SEARCH_PRODUCTS: {
                    activity.query = req.body
                    break
                }

                default:
                    break
            }

            const SEARCH_ACTIVITY_TOPIC = config.get('kafka.producer.topicDefault')[0].topic
            kafkaProducer.send(SEARCH_ACTIVITY_TOPIC, activity)
        }
    }
    next()
}

const trackingRoutes = [
    //products
    {
        method: 'get',
        path: '/products/:productId',
        type: constants.GET_PRODUCT_BY_ID,
    },
    {
        method: 'post',
        path: '/products/search',
        type: constants.SEARCH_PRODUCTS
    },
]