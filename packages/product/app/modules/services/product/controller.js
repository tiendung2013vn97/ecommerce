const express = require("express");
const { asyncHandler } = require('@ntiendung/util');
const router = express.Router();
const handler = require("./handler")
const { validateQueryProductsReq } = require('./validator')

//validate request
router.post("/search", validateQueryProductsReq)

// get product by id
router.get("/:productId", asyncHandler(async (req, res, _) => handler.getProductById(req)))

//search product by query
router.post("/search", asyncHandler(async (req, res, _) => handler.searchProducts(req)))

module.exports = router