const { CommonValidator, constants, response } = require("@ntiendung/util")
const joi = require("joi")



//validate request input of api POST "/products/query"
function validateQueryProductsReq(req, res, next) {
    const schema = joi.object({
        query: joi.object(),
        from: joi.number().integer(),
        size: joi.number().integer(),
    }).required()

    //validate request input
    const validationResult = CommonValidator.validateSchema(schema, req.body)

    //throw error 400 if there are any invalid attributes in request input
    if (validationResult.errors.length) return res.status(400).json(response.getErrResp(constants.VALIDATION_ERROR, undefined, validationResult.errors))

    req.body = validationResult.value
    next()
}

module.exports = {
    validateQueryProductsReq
}
