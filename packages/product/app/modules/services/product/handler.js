const es = require('../../elasticsearch')
const { response } = require('@ntiendung/util')
const constants = require('../../config/constants')

async function getProductById(req) {
    return es.searchProductById(req.params.productId).catch(err => {
        if (err === 'NOT_FOUND') throw response.getErrResp(constants.PRODUCT_NOT_FOUND, undefined, undefined, 404)
    })
}

async function searchProducts(req) {
    return es.searchProducts(req.body)
}

module.exports = {
    searchProducts,
    getProductById
}