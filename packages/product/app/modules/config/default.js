module.exports = {
    jwt: {
        privateKey: "-----BEGIN RSA PRIVATE KEY-----\nMIICWgIBAAKBgGyf/vEDiwP4NyTgIAi9TyGoJkdTil9gQujjHV0466FM7EiLPzqK\nJr0u239WudrW2nNmkVQVPCKzM+uYrcTOY+EgLxf33x+UXwYt7u0130XuHPbqRpSP\npea/B3N5A+HRgO2X6wCYHws2tvXJZsX5I0G/RA383B/y9y5yOTrFmXmHAgMBAAEC\ngYAcmyn9eKxkeDq1eFyXUJuFRo4GHKcFJQmZ4S27fiD3kpVrKViWZwl6qttdNo6t\nZgWTfIBrP0hd7wmOUt/4Q04kAceMko3TjdkvFYJdA8Zu0JH+8fIP+2yTNZdPO/f8\nZF1FAHLALHukAjd+R5OQKpoEoQ9JJl9vN1aBUVchOfI2SQJBANYyaF1pRE8U2DOk\nX2shAlUHUV8NnVFVWDbC8W3XMTL2bHRw5eYQVG2FSmhka9Qs2NTGH/kL3iN3KJ01\n7+TTYsMCQQCB0w9NpmcemEK7v9RR8BvFkjC8cBIkmeGFAvMNCk1StKs1bapGIvBP\nThyJo3/7AzUHkMd4KyEQIYNVxSY5lhntAkBp0aNfqpa1nu2krar6KOeXDDN2CyrS\nOKCJBUvy4OIMdFNmPzREsYKUTMpSwnHG85T3oR3Aks1epgqpiDEI0imzAkBthJUm\nVjgrkRrKs7rzHHDxLLA8ZvaHNeEPijtrRsOP/6VmvwqjfBnpbOWaHvl50WzM4w/C\nie+sYfkfQ99JORzJAkA2rXL0rgpHsNA77EGK5hjR6goCMBVDq6l0lLz7hdhPtEqE\nYX8tI7X+baBko+T895i0qRakG5ojNAfRXvuXqkQ7\n-----END RSA PRIVATE KEY-----",
        publicKey: "-----BEGIN PUBLIC KEY-----\nMIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGyf/vEDiwP4NyTgIAi9TyGoJkdT\nil9gQujjHV0466FM7EiLPzqKJr0u239WudrW2nNmkVQVPCKzM+uYrcTOY+EgLxf3\n3x+UXwYt7u0130XuHPbqRpSPpea/B3N5A+HRgO2X6wCYHws2tvXJZsX5I0G/RA38\n3B/y9y5yOTrFmXmHAgMBAAE=\n-----END PUBLIC KEY-----"
    },
    kafka: {
        server: {
            kafkaHost: process.env.KAFKA_HOST||'localhost:29092'
        },
        consumer: {
            option: {
                autoCommit: process.env.KAFKA_AUTO_COMMIT || true,
                fetchMaxWaitMs: process.env.KAFKA_FETCH_MAX_WAITS || 1000,
                fetchMaxBytes: process.env.KAFKA_FETCH_MAX_BYTES || 1024 * 1024,
                encoding: process.env.KAFKA_ENCODING || "utf8",
                fromOffset: process.env.KAFKA_FROM_OFFSET || false
            },
            fetch_requests: [
                {
                    topic: process.env.SEARCH_ACTIVITY || 'SEARCH_ACTIVITY',
                    partitions: process.env.KAFKA_PARTITION || 1
                }
            ]
        },
        producer: {
            option: {
                requireAcks: 1,
                ackTimeoutMs: 100,
                partitionerType: 0
            },
            topicDefault: [
                {
                    topic: process.env.SEARCH_ACTIVITY || 'SEARCH_ACTIVITY',
                    partitions: process.env.KAFKA_PARTITION || 1,
                    replicationFactor: process.env.REPLICATE_FACTOR_DEFAULT || 2
                }
            ]
        }
    }
}