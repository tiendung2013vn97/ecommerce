module.exports = [
    //products
    {
        method: 'get',
        path: '/products/:productId',
        requireAccessToken: 'optional' //enum [mandatory,optional]
    },
    {
        method: 'post',
        path: '/products/search',
        requireAccessToken: 'optional' //enum [mandatory,optional]
    },
]