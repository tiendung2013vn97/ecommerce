
const { Client } = require('@elastic/elasticsearch');
const { logger } = require("@ntiendung/logger")

let es
//_________________________________________________________________________________________________
//config elasticsearch connection
async function initElastic() {
    try {
        logger.info(`Start initializing elasticsearch credentials at ${process.env.ES_DOMAIN}...`)

        es = new Client({
            node: process.env.ES_DOMAIN,
            maxRetries: 5,
            requestTimeout: 5000,
            sniffOnStart: false
        });
        await es.indices.getSettings()
        logger.info(`Setup elasticsearch credentials done!`)
    } catch (error) {
        logger.error(`Failed to initialize elasticsearch. `, { Reason: error })
        throw `Failed to initialize elasticsearch`
    }
}

//init elasticsearch settings, exit program if failed
initElastic().catch(() => process.env.NODE_ENV == "production" && process.exit(1))

//_________________________________________________________________________________________________
async function searchProducts(body) {
    return es.search({
        index: process.env.ES_PRODUCT_INDEX, body
    }).then(result => ({
        products: result.body.hits.hits.map(item => item._source),
        from: body.from,
        size: body.size,
        total: result.body.hits.total.value
    }))
}

async function searchProductById(productId) {
    return es.get({
        index: process.env.ES_PRODUCT_INDEX,
        id: productId
    }).then(result => result.body._source)
        .catch(err => {
            if (err?.meta?.statusCode === 404) throw 'NOT_FOUND'
            else throw err
        })
}



module.exports = {
    searchProducts,
    searchProductById
}
