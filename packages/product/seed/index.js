const { Client } = require('@elastic/elasticsearch');
const products = require('./products');
require("dotenv").config();


(async function createNewProducts() {
    setTimeout(async () => {
        try {
            es = new Client({
                node: process.env.ES_DOMAIN,
                maxRetries: 5,
                requestTimeout: 5000,
                sniffOnStart: false
            });

            const bulkBody = products.flatMap((product, index) => [{ update: { _index: process.env.ES_PRODUCT_INDEX, _id: `${index}` } }, { doc: product, doc_as_upsert: true }])

            const { body: bulkResponse } = await es.bulk({ body: bulkBody })
            //filter operations that is not successful with returned status differ from 200,201
            const errors = bulkResponse.items.filter(item => ![200, 201].includes(Object.values(item)[0].status))
            if (errors.length) {
                console.info(`ES bulk request is completed with errors:`, JSON.stringify(errors))
            } else {
                console.info(`ES bulk request is completed with no errors.`)
            }

        } catch (error) {
            console.error(`Failed to create new products. Reason:`, error)
        }
    }, 5000)
})()