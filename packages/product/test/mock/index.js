const services = {
    api: require("./api")
}

module.exports = {
    //mock before each testcase run
    mock: async function (mocks) {
        mocks.forEach(mockData => {
            if (!services[mockData.service]) throw `Service ${mockData.service} isn't supported to mock.`
            services[mockData.service].mock(mockData)
        })
        return "mocked"
    },
    //unmock after each testcase finished
    unmock: async function (mocks) {
        mocks.forEach(mockData => {
            if (!services[mockData.service]) return
            services[mockData.service].unmock(mockData)
        })
        return "unmocked"
    },
    //initialize before running any testcases
    beforeAll: function () {
        Object.values(services).forEach(service => {
            if (service.beforeAll) service.beforeAll()
        })
        return "initialized"
    }
}