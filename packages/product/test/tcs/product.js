const { testUnit } = require("../run")
const modulePath = "app/modules/services/product/handler"

describe("Product testcases", async () => {
    testUnit("Get product by id successfully", { modulePath, funcName: "getProductById", key: "product/get-product-by-id-v1" })
    testUnit("Search products successfully", { modulePath, funcName: "searchProducts", key: "product/search-products-v1" })
})
