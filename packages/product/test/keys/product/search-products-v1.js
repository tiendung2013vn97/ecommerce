const config = require("../../config")
module.exports = {
    "input": {
        "args": [
            {
                "body": {
                    "from": 0,
                    "size": 1
                }
            }
        ]
    },
    "output": {
        "data": {
            products: [
                {
                    "name": "Hades",
                    "imageUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/1145360/ss_8a9f0953e8a014bd3df2789c2835cb787cd3764d.1920x1080.jpg?t=1624463563",
                    "description": "Defy the god of the dead as you hack and slash out of the Underworld in this rogue-like dungeon crawler from the creators of Bastion, Transistor, and Pyre.",
                    "price": 10,
                    "countInStock": 15,
                    "release": "17 Sep, 2020",
                    "reviews": "Very Positive",
                    "developer": "Supergiant Games",
                    "systemReq": [
                        "ios",
                        "windows"
                    ],
                    "genres": [
                        "Action Roguelike",
                        "Indie",
                        "RPG"
                    ],
                    "aboutUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/1145360/extras/HadesSteam_1.0D.gif?t=1624463563",
                    "about": "Hades is a god-like rogue-like dungeon crawler that combines the best aspects of Supergiant's critically acclaimed titles, including the fast-paced action of Bastion, the rich atmosphere and depth of Transistor, and the character-driven storytelling of Pyre. BATTLE OUT OF HELL: As the immortal Prince of the Underworld, you'll wield the powers and mythic weapons of Olympus to break free from the clutches of the god of the dead himself, while growing stronger and unraveling more of the story with each unique escape attempt. UNLEASH THE FURY OF OLYMPUS: The Olympians have your back! Meet Zeus, Athena, Poseidon, and many more, and choose from their dozens of powerful Boons that enhance your abilities. There are thousands of viable character builds to discover as you go."
                }
            ],
            "from": 0,
            "size": 1,
            "total": 10

        },
        "isErr": false
    },
    "mocks": [
        {
            "service": "api",
            "action": {
                "baseUrl": config.elastic.domain,
                "method": "post",
                "path": `/${config.searchIndex}/_search`
            },
            "output": {
                "code": 200,
                "data": {
                    "hits": {
                        "total": {
                            "value": 10,
                            "relation": "eq"
                        },
                        "max_score": 1.0,
                        "hits": [
                            {
                                "_index": "product",
                                "_type": "_doc",
                                "_id": "0",
                                "_score": 1.0,
                                "_ignored": [
                                    "about.keyword"
                                ],
                                "_source": {
                                    "name": "Hades",
                                    "imageUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/1145360/ss_8a9f0953e8a014bd3df2789c2835cb787cd3764d.1920x1080.jpg?t=1624463563",
                                    "description": "Defy the god of the dead as you hack and slash out of the Underworld in this rogue-like dungeon crawler from the creators of Bastion, Transistor, and Pyre.",
                                    "price": 10,
                                    "countInStock": 15,
                                    "release": "17 Sep, 2020",
                                    "reviews": "Very Positive",
                                    "developer": "Supergiant Games",
                                    "systemReq": [
                                        "ios",
                                        "windows"
                                    ],
                                    "genres": [
                                        "Action Roguelike",
                                        "Indie",
                                        "RPG"
                                    ],
                                    "aboutUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/1145360/extras/HadesSteam_1.0D.gif?t=1624463563",
                                    "about": "Hades is a god-like rogue-like dungeon crawler that combines the best aspects of Supergiant's critically acclaimed titles, including the fast-paced action of Bastion, the rich atmosphere and depth of Transistor, and the character-driven storytelling of Pyre. BATTLE OUT OF HELL: As the immortal Prince of the Underworld, you'll wield the powers and mythic weapons of Olympus to break free from the clutches of the god of the dead himself, while growing stronger and unraveling more of the story with each unique escape attempt. UNLEASH THE FURY OF OLYMPUS: The Olympians have your back! Meet Zeus, Athena, Poseidon, and many more, and choose from their dozens of powerful Boons that enhance your abilities. There are thousands of viable character builds to discover as you go."
                                }
                            }]
                    }
                }
            }
        }
    ]
}