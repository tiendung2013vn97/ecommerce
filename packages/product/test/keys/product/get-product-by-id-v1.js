const config = require("../../config")
module.exports = {
    "input": {
        "args": [
            {
                "params": {
                    "productId": "1"
                }
            }
        ]
    },
    "output": {
        "data": {
            "name": "Bastions",
            "imageUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/107100/ss_7d3cd935098d3cd0603f713017624ef508c86c98.1920x1080.jpg?t=1601950406",
            "description": "Discover the secrets of the Calamity, a surreal catastrophe that shattered the world to pieces.",
            "price": 7,
            "countInStock": 40,
            "release": "17 Aug, 2011",
            "reviews": "Very Positive",
            "developer": "Supergiant Games",
            "systemReq": [
                "ios",
                "windows"
            ],
            "genres": [
                "Action",
                "Indie",
                "Narration"
            ],
            "aboutUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/107100/extras/preview-full-Bastion_Combat1.png?t=1601950406",
            "about": "Bastion is an action role-playing experience that redefines storytelling in games, with a reactive narrator who marks your every move. Explore more than 40 lush hand-painted environments as you discover the secrets of the Calamity, a surreal catastrophe that shattered the world to pieces. Wield a huge arsenal of upgradeable weapons and battle savage beasts adapted to their new habitat. Finish the main story to unlock the New Game Plus mode and continue your journey!"
        },
        "isErr": false
    },
    "mocks": [
        {
            "service": "api",
            "action": {
                "baseUrl": config.elastic.domain,
                "method": "get",
                "path": `/${config.searchIndex}/_doc/1`
            },
            "output": {
                "code": 200,
                "data": {
                        _source: {
                            "name": "Bastions",
                            "imageUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/107100/ss_7d3cd935098d3cd0603f713017624ef508c86c98.1920x1080.jpg?t=1601950406",
                            "description": "Discover the secrets of the Calamity, a surreal catastrophe that shattered the world to pieces.",
                            "price": 7,
                            "countInStock": 40,
                            "release": "17 Aug, 2011",
                            "reviews": "Very Positive",
                            "developer": "Supergiant Games",
                            "systemReq": [
                                "ios",
                                "windows"
                            ],
                            "genres": [
                                "Action",
                                "Indie",
                                "Narration"
                            ],
                            "aboutUrl": "https://cdn.cloudflare.steamstatic.com/steam/apps/107100/extras/preview-full-Bastion_Combat1.png?t=1601950406",
                            "about": "Bastion is an action role-playing experience that redefines storytelling in games, with a reactive narrator who marks your every move. Explore more than 40 lush hand-painted environments as you discover the secrets of the Calamity, a surreal catastrophe that shattered the world to pieces. Wield a huge arsenal of upgradeable weapons and battle savage beasts adapted to their new habitat. Finish the main story to unlock the New Game Plus mode and continue your journey!"
                        }
                }
            }
        }
    ]
}