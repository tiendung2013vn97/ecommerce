module.exports = {
    diff: true,//mocha try to show differences beetween expected and fail testcase
    extension: ['js'], //files having this extension will be considered test files
    reporter: 'mochawesome',//use mochawesome as reporter
    slow: 100,//highlight testcase whose performing time greater than "slow"(ms) and mark this testcase as failure
    timeout: 5000, //exceed timeout(ms) then testcase marked as failure
    ui: 'bdd', //use bdd syntax to write testcase
    recursive: true, //find test file in subfolder when recursive is true
    "reporter-options": [
        // `reportFilename=productTestReport-${Date.now()}`, //report filename
        `reportFilename=productTestReport`, //report filename
        `reportDir=test/product-reports`, //report directory
        'json=false', // not export to file json
    ],
    exit: true,//Force Mocha to quit after tests complete
    parallel: false,//Run tests in parallel
    spec: ["./test/tcs/**"],//Specify where testcase found to run
    grep: process.env.TEST_GREP || ".*", //Only run tests matching this string or regexp -> Useful when you only want to run certain test cases
    
};
