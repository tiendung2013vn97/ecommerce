const { JWT } = require('@ntiendung/util')

async function login(req) {
    //just hardcode a user for the demo
    const user = {
        userId: "user-id-123",
        name: "Dũng Nguyễn"
    }
    const accessToken = await JWT.encrypt(user)
    const refreshToken = 'ABCDEF'

    return {
        refreshToken,
        accessToken
    }
}

module.exports = {
    login
}