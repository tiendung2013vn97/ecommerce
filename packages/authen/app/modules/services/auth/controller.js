const express = require("express");
const { asyncHandler } = require('@ntiendung/util');
const router = express.Router();
const handler = require("./handler")

// login
router.post("/login", asyncHandler(async (req, res, _) => {
    const { accessToken, refreshToken } = await handler.login(req)
    res.cookie('authorization', `Bearer ${accessToken}`, { httpOnly: true });
    res.cookie('refreshToken', refreshToken, { httpOnly: true });
    res.send({ status: 'Logged in successfully!', accessToken, refreshToken })
}))

module.exports = router