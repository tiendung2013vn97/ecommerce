global.ROOT = __dirname;
const { loadConf } = require('@ntiendung/util').config;
const { httpLoggerMdw, errorHandlerMdw } = require('@ntiendung/middlewares');
const { httpLogger } = require('@ntiendung/logger');
const express = require('express');

const app = express();
const cors = require('cors');
const helmet = require('helmet');
const path = require('path');

// use cors middleware to avoid error CROSS DOMAIN
app.use(helmet());
app.use(cors());

app.use(httpLoggerMdw.createLoggerMdw(httpLogger));

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false, limit: '50mb' }));

app.use(express.static(path.join(__dirname, '../public')));

// init base configurations
loadConf({ loadFromDb: false }).then(async () => {
  // load route
  const VERSION = process.env.VERSION;
  app.use(`/api/${VERSION}`, require('./modules/routes'));

  // the latest layer to handle response before sending to client
  app.use(errorHandlerMdw);
});

module.exports = app;
