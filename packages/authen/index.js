#!/usr/bin/env node
const assert = require('assert');

require('dotenv').config();

const { logger } = require('@ntiendung/logger');

// Constants
const PORT = process.env.PORT || 3001;

async function initServer() {
  // sleep for 1s for elasticbeanstalk log to catch up
  await new Promise((resolve) => {
    setTimeout(resolve, 1000);
  });

  const app = require('./app');
  const server = app.listen(PORT, function () {
    logger.info(`Webserver is ready and listening on port ${PORT}!`);
    logger.info(`Running mode: ${process.env.NODE_ENV}`);
    logger.info(`Commit id   : ${process.env.COMMIT_ID}`);
    logger.info(`Build time  : ${process.env.BUILD_TIME}`);
    logger.info(`Branch      : ${process.env.BRANCH}`);
    logger.info(`Env         : ${process.env.ENV}`);
  });

  //
  // need this in docker container to properly exit since node doesn't handle SIGINT/SIGTERM
  // this also won't work on using npm start since:
  // https://github.com/npm/npm/issues/4603
  // https://github.com/npm/npm/pull/10868
  // https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
  // if you want to use npm then start with `docker run --init` to help, but I still don't think it's
  // a graceful shutdown of node process
  //

  // quit on ctrl-c when running docker in terminal
  process.on('SIGINT', function onSigint() {
    logger.info(`Got SIGINT (aka ctrl-c in docker). Graceful shutdown ${new Date().toISOString()}`);
    // eslint-disable-next-line
        shutdown();
  });

  // quit properly on docker stop
  process.on('SIGTERM', function onSigterm() {
    logger.info(`Got SIGTERM (docker container stop). Graceful shutdown ${new Date().toISOString()}`);
    // eslint-disable-next-line
        shutdown();
  });

  // process unhandled promise rejection
  process.on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection at:', { promise, reason });
    if (process.env.NODE_ENV !== 'production') {
      assert.fail('There is an unhandled rejection!');
    }
  });

  const sockets = {};
  let nextSocketId = 0;
  server.on('connection', function (socket) {
    const socketId = nextSocketId++;
    sockets[socketId] = socket;

    socket.once('close', function () {
      delete sockets[socketId];
    });
  });

  // shut down server
  function shutdown() {
    // eslint-disable-next-line
        waitForSocketsToClose(10);

    server.close(function onServerClosed(err) {
      if (err) {
        // eslint-disable-next-line
                console.error(err);
        process.exitCode = 1;
      }
      process.exit();
    });
  }

  function waitForSocketsToClose(counter) {
    if (counter > 0) {
      logger.info(
        `Waiting ${counter} more ${counter === 1 ? 'seconds' : 'second'} for all connections to close...`
      );
      return setTimeout(waitForSocketsToClose, 1000, counter - 1);
    }

    logger.info('Forcing all connections to close now');
    for (const socketId in sockets) {
      sockets[socketId].destroy();
    }
  }
}

initServer();
