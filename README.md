# Ecommerce App

## Description
Project use **Microservice Architecture** with **3 main services**:
* **Authorization Serivce**: reponsible for user authentication, I build this service for purpose of tracking user's searching activities.
* **Product Service**: provide searching products APIs for user.
* **Report Service**: this service will summary user's searching activies on Product Service and allow user to query user's activities.

Manage code structure with **Monorepo** architecture (Lerna) and **MVC** model. 
## Flow Diagram
![image](https://drive.google.com/uc?export=view&id=1c6m7gFe55OFcpQnJBOAG2MfRC1NUR2B_)

## Tech
* Nodejs (ES6)
* Kafka
* Elasticsearch
* Docker
* Lerna
## Installation
```sh
https://gitlab.com/tiendung2013vn97/ecommerce.git
cd ecommerce
docker-compose up -d
```
Then wait at least 3 minutes for all services to available. After that you can test APIs (use curl or Postman):
+ Login: 
```sh
curl --location --request POST 'localhost:3001/api/v1/auth/login'
```
+ Get product by id: 
```sh
curl --location --request GET 'localhost:3002/api/v1/products/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from":0,
    "size":100
}'
```
+ Query products: 
```sh
curl --location --request POST 'localhost:3002/api/v1/products/search' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from":0,
    "size":100
}'
```
+ Then you can check user's searching activities:
```sh
curl --location --request POST 'localhost:3003/api/v1/report/search-activities/query' \
--header 'Content-Type: application/json' \
--data-raw '{
    "size":100
}'
```
## Test
To run test, following these steps:
```sh
cd ecommerce
yarn 
yarn install:packages
yarn test
```
